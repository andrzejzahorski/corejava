package equals;

import java.time.LocalDate;
import java.util.Objects;

public class Employee {
    /*Przepis na metodę equals:
    * 1. Zwrotność: x.equals(x) -> true
    * 2. Symetria: dla dow. x i y x.eq(y) -> true <-> y.eq(x) -> true
    * 3. Przechodniość: dla dow x,y,z: x.eq(y) = true i y.eq(z) = true -> x.eq(z) = true
    * 4. Niezmienność: Jeżeli obiekty nie zmieniły sięw czasie to kolejne wywołania metody x.eq(y) zwrócą tą samą wartość
    * 5. Dla każdego x != null, wywołanie x.eq(null) zwraca wartość false
    * */

    private String name;
    private double salary;
    private LocalDate hireDay;

    public Employee(String name, double salary, int year, int month, int day) {
        this.name = name;
        this.salary = salary;
        this.hireDay = LocalDate.of(year, month, day);
    }

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    public LocalDate getHireDay() {
        return hireDay;
    }

    public void raiseSalary(double percent) {
        double raise = salary * percent / 100;
        salary += raise;
    }

    @Override
    public boolean equals(Object otherObj) {
        if (this == otherObj) return true;

        //check if otherObj is null
        if (otherObj == null) return false;

        //if classes don't match, they shouldn't be equal
        if (this.getClass() != otherObj.getClass()) return false;

        // we now know that, otherObj is non-null Employee
        Employee other = (Employee) otherObj;

        return Objects.equals(this.name, other.name) && this.salary == other.salary
                && Objects.equals(this.hireDay, other.hireDay);
    }
}
