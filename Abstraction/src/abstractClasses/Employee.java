package abstractClasses;

import java.time.LocalDate;

public class Employee extends Person {

    private double salary;
    private LocalDate hireday;

    public Employee(String name, double salary, int year, int month, int day) {
        super(name);
        this.salary = salary;
        hireday = LocalDate.of(year, month, day);
    }

    public double getSalary() {
        return salary;
    }

    public LocalDate getHireday() {
        return hireday;
    }

    @Override
    public String getDescription() {
        return String.format("employee with salary: $%.2f", salary);
    }

    public  void raiseSalary(double percent) {
        double raise = salary * percent / 100;
        salary = salary + raise;
    }
}
