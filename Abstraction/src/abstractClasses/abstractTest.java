package abstractClasses;

public class abstractTest {
    public static void main(String[] args) {
        Person[] people = new Person[3];

        people[0] = new Employee("Obi Wan", 12345, 1985, 4, 4);
        people[1] = new Employee("Luke Skywalker", 320, 1988, 4,5);
        people[2] = new Employee("Han Solo", 3200, 1982, 11,5);

        for (Person p : people) {
            System.out.println(p.getName() + ", " + p.getDescription());
        }

    }
}
